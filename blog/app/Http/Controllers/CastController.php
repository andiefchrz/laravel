<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
      public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
        ]);
        return redirect('/cast');
    }

	  public function index()
	    {
	        // $post = DB::table('cast')->get();
	        return view('cast.index');
	    }
}
