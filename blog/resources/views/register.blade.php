@extends('layout.master')

@section('title')
  Pendaftaran
@endsection

@section('content')
<body>
	<h1>Buat Account Baru!</h1>
		<h2>Sign Up Form</h2>
	<table>
		<form action="/welcome">
			<p>
				<label>First name:</label>
			</p>
				<input type="text" name="username">
			<p>
				<label>Last name:</label>
			</p>
				<input type="text" name="username">
			<p>Gender:</p>
				<p><input type="radio" name="Gender" value="Male">Male</p>
				<p><input type="radio" name="Gender" value="Female">Female</p>
				<p><input type="radio" name="Gender" value="Other">Other</p>
			<p>Nationality:</p>
				<select name="natinality">
					<option value="Indonesia">Indonesia</option>
					<option value="Malaysia">Malaysia</option>
					<option value="Singapore">Singapore</option>
				</select>
			<p>Language Spoken:</p>
				<p><input type="checkbox" name="language" value="indonesia">Bahasa Indonesia</p>
				<p><input type="checkbox" name="language" value="english">English</p>
				<p><input type="checkbox" name="language" value="other">Other</p>
			<p>Bio:</p>
				<tr>
					<td><textarea cols="30" rows="10"></textarea></td>
				</tr>
				<tr>
					<td><input type="submit" value="Sign Up"></td>
				</tr>
			</form>
	</table>
@endsection